# FNWI meeting on research data


> info & data
>
> storage & backup
>
> current & future

Peter van Campen, Bram Daams, Simon Oosthoek and Bjorn Bellink, C&CZ

June 28, 2018

> URL [https://wiki.science.ru.nl/cncz/dataslides/](https://wiki.science.ru.nl/cncz/dataslides/)

---

# Outline
> Storage at C&CZ and future
>
> Backup at C&CZ and future
>
> Other solutions in use and future
>
> Lab journals (at C&CZ?)
>
> Code hosting at C&CZ
>
> Group discussion
>
> Drinks
>

### Please feel free to interrupt with questions/remarks!
 
---


# Storage at C&CZ and future

- For projects/departments (rented or owned): currently 182 TB of 500 TB filled on 18 servers + Astro: 387 of 627 TB on 3 servers
> smb://project-srv.science.ru.nl/project as never-changing URL
>
> DIY user management by project owners
>
> Access rights (read/write/access) per project

## Costs

|    |    |
|----|----|
| Currently |  50 €/TB/year (since July 2018) |
| New large scale (>2000 TB) | 25 €/TB/year at other uni's |

---

# Backup at C&CZ and future
- Own: Synchronization to 2nd own server, with snapshots. Datacenter network will be upgraded in 2018 so servers can be distanced
- Rented: more versions, more elaborate choices and schedules:
	- Daily incrementals after working day.
		-   Currently: Each day: 7.5 TB on tape, totaling 150 TB
		-   Soon: disk-backup: faster, deduplication, flexible retention vs. tape rotation. Cheaper? Now: 70 €/TB/year
	- Monthly backups can then become Bi-Monthly backups (?)
		-   Currently: Each weekend. Per month: 50 TB, totaling 600 TB
		-   Now: Monthly 60 €/TB/year Soon: Bi-Monthly (?). 2x cheaper. 
	- Yearly backups for archive
		-   Latest Yearly: 78 TB. Now: 25 €/TB/year.

---

# Other Storage and Backup solutions in use and future

| Product                      | Limits    | Costs                              |
|:-----------------------------|:----------|:-----------------------------------|
|RIS (FAIR)                    |  Limited    | 0 €/TB/year                        |
|SURFdrive  |0.25TB  | 0 €/TB/year                        |
|Google Drive File Stream      | ∞       | 0 €/TB/year                        | 
|Hard discs in drawers, ex-PhD knows what&where| ∞  | 10 €/TB/year       |
|Amazon Glacier (EU/Ireland)   | ∞       | 48 $/TB/year + costs for retrieval |
|Radboud.Data                  | ?         | end 2018 financing more clear, ? 480 €/TB/year ?      |
|ISC NAS \\\\cnas.ru.nl\wrkgrp |  ∞       | 1100 €/TB/year (2019: 900 €/TB/year)                     |
|4TU.ResearchData              |   ∞         |0.01 TB free, 4050 €/TB for more  |
---
# Lab journals

| Product                                    | Data                       |           |
| -------------------------------------------|----------------------------|-----------|
| [OneNote](http://www.onenote.com)          | Depending on version       | [Hints for using Microsoft OneNote as lab journal](https://www.ascb.org/compass/compass-points/11-tricks-for-using-onenote-as-your-lab-notebook/) |
| [Edugroepen](https://www.edugroepen.nl)    | Surf (NL/EU?)              | Based on Microsoft Sharepoint |
| [SciNote](https://scinote.net/)            | On premise / Private cloud | Open source; 360 $/user/year for support |
| [eLabFTW](https://www.elabftw.net)         | On premise                 | Open source; Login with U-number should be possible [Demo](https://elabftw.science.ru.nl)  |
| [Jupyter](https://jupyter.org) / [JupyterHUB](https://jupyterhub.readthedocs.io/en/stable/) | On premise                 | Open source; Login with science account should be possible, needs further investigation |

---

# Code hosting platform GitLab
- On premise C&CZ service running [GitLab](https://gitlab.com) Open Source software
- Introduced Jan. 2015
- Stats: 1700+ projects | 120 groups | 900+ staff and students | 100+ external users

Features:
* Suitable for code, scripts, documentation, theses, figures etc.
* Git repository management
* Issue board
* Code reviewing
* Wiki pages
* Git-LFS support
* Private, internal or public permissions on projects
* Fixed URL / namespaces for projects
* Daily, Monthly and Yearly backups of all content

---
# Group discussion & drinks

